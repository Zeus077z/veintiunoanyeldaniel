﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using veintiuno_utn.Entities;

namespace veintiuno_utn.DAL
{
    public class MesaDAL
    {
        /// <summary>
        /// Actualiza el estado de una mesa
        /// </summary>
        /// <param name="m">Objeto mesa</param>
        public void PartidaEstado(EMesa m)
        {
            try
            {
                string sql = "UPDATE mesas SET partida_en_curso = @part WHERE id = @id";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@part", m.Partida_en_curso);
                    cmd.Parameters.AddWithValue("@id", m.Id);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Problema en update: " + ex);
            }
        }
        /// <summary>
        /// Actualiza los jugadores en una mesa
        /// </summary>
        /// <param name="m">Mesa</param>
        public void Jugadores(EMesa m)
        {
            try
            {
                string sql = "UPDATE mesas SET jugadores_en_mesa = @jug WHERE id = @id";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@jug", m.Jugadores_en_mesa);
                    cmd.Parameters.AddWithValue("@id", m.Id);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Problema en update: " + ex);
            }
        }
        /// <summary>
        /// Verifica una mesa
        /// </summary>
        /// <param name="m">objeto mesa</param>
        /// <returns>booleano</returns>
        public bool VerificarMesa(EMesa m)
        {
            string sql = "SELECT id, id_invitacion, pass, cantidad_max_jug, jugadores_en_mesa," +
                " ganador_id, partida_en_curso FROM mesas WHERE partida_en_curso = true and jugadores_en_mesa < 6";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);                
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    EMesa temp = CargarMesa(reader);
                    m.Id = temp.Id;
                    m.Codigo_invitacion = temp.Codigo_invitacion;
                    m.Pass = temp.Pass;
                    m.Cantidad_max_jug = temp.Cantidad_max_jug;
                    m.Jugadores_en_mesa = temp.Jugadores_en_mesa;
                    m.Ganador = temp.Ganador;
                    m.Partida_en_curso = temp.Partida_en_curso;
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Carga los datos de una mesa
        /// </summary>
        /// <param name="reader">reader</param>
        /// <returns>Objeto mesa</returns>
        private EMesa CargarMesa(NpgsqlDataReader reader)
        {
            EMesa e = new EMesa()
            {
                Id = reader.GetInt32(0),
                Codigo_invitacion = reader.GetString(1),
                Pass = reader.GetString(2),
                Cantidad_max_jug = reader.GetInt32(3),
                Jugadores_en_mesa = reader.GetInt32(4),
                Ganador = reader.GetInt32(5),
                Partida_en_curso = reader.GetBoolean(6)
            };
            return e;
        }
        /// <summary>
        /// Carga una lista de un usuarios que están en una mesa 
        /// </summary>
        /// <param name="m">Mesa</param>
        /// <returns>lista usuarios</returns>
        public List<int> Usuarios(EMesa m)
        {
            List<int> registros = new List<int>();
            int id = 0;
            string sql = "SELECT id_usuario FROM partida WHERE id_mesa = @id and id_usuario > 0 ORDER BY num_jugador";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", m.Id);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    id = !reader.IsDBNull(0) ? reader.GetInt32(0) : id = 0;
                    registros.Add(id);
                }
            }
            return registros;
        }
      
   
      
       

    }
}
