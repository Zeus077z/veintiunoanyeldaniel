﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using veintiuno_utn.Entities;

namespace veintiuno_utn.DAL
{
    public class CartaDAL
    {
        /// <summary>
        /// Inserta una carta a la base de datos
        /// </summary>
        /// <param name="carta">Objeto Carta</param>
        /// <returns><int/returns>
        public int Insertar(ECarta carta)
        {
            try
            {
                string sql = "INSERT INTO cartas (id_jugador, carta, deck_id, id_mesa, valor_num) " +
                    "VALUES (@id_jug, @carta, @deck, @id_mesa, @valor) returning id; ";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@id_jug", carta.Id_jugador);
                    cmd.Parameters.AddWithValue("@carta", carta.Carta);
                    cmd.Parameters.AddWithValue("@deck", carta.Deck_id);
                    cmd.Parameters.AddWithValue("@id_mesa", carta.Id_Mesa);
                    cmd.Parameters.AddWithValue("@valor", carta.Valor_num);
                    int id = (int)cmd.ExecuteScalar();
                    return id;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Carga una carta
        /// </summary>
        /// <param name="cart">Objeto carta</param>
        /// <returns></returns>
        public ECarta CargarCartas(ECarta cart)
        {
            string sql = "SELECT id, id_jugador, carta, deck_id, id_mesa, valor_num FROM cartas WHERE id_mesa = @id_mesa and id_jugador = @id_jug ORDER BY id ASC LIMIT 1";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id_mesa", cart.Id_Mesa);
                cmd.Parameters.AddWithValue("@id_jug", cart.Id_jugador);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    ECarta temp = CargarCarts(reader);
                    cart.Id = temp.Id;
                    cart.Id_jugador = temp.Id_jugador;
                    cart.Carta = temp.Carta;
                    cart.Deck_id = temp.Deck_id;
                    cart.Id_Mesa = temp.Id_Mesa;
                    cart.Valor_num = temp.Valor_num;
                    return cart;
                }
            }
            return cart;
        }
        /// <summary>
        /// Carga las cartas del dealer
        /// </summary>
        /// <param name="carta">Objeto carta</param>
        /// <returns>Lista de cartas</returns>
        public List<ECarta> CargarCartasDealer(ECarta carta)
        {
            List<ECarta> cart = new List<ECarta>();
            string sql = "SELECT id, id_jugador, carta, deck_id, id_mesa, valor_num FROM cartas WHERE id_mesa = @id_mesa and id_jugador = @id_jug";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id_mesa", carta.Id_Mesa);
                cmd.Parameters.AddWithValue("@id_jug", carta.Id_jugador);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    cart.Add(CargarCarts(reader));                    
                }
            }
            return cart;
        }
        /// <summary>
        /// Obtiene la información de  una carta
        /// </summary>
        /// <param name="reader">reader</param>
        /// <returns>Objeto carta</returns>
        private ECarta CargarCarts(NpgsqlDataReader reader)
        {
            ECarta e = new ECarta()
            {
                Id = reader.GetInt32(0),
                Id_jugador = reader.GetInt32(1),
                Carta = reader.GetString(2),
                Deck_id = reader.GetInt32(3),
                Id_Mesa = reader.GetInt32(4),
                Valor_num = reader.GetInt32(5)
            };
            return e;
        }
        /// <summary>
        /// Obtiene suma total de las cartas de un jugador
        /// </summary>
        /// <param name="c">Objeto carta</param>
        /// <returns>int total</returns>
        public int TotalValor(ECarta c)
        {
            int total = 0;
            string sql = "select sum(valor_num) from cartas where id_jugador = @id_jug and id_mesa = @id_mesa ";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id_jug", c.Id_jugador);
                cmd.Parameters.AddWithValue("@id_mesa", c.Id_Mesa);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    total = !reader.IsDBNull(0) ? reader.GetInt32(0) : total = 0;
                }
            }
            return total;
        }
    }
}
