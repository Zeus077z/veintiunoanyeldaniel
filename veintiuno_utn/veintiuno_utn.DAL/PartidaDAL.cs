﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using veintiuno_utn.Entities;

namespace veintiuno_utn.DAL
{
    public class PartidaDAL
    {
        public EPartida VerificarTurno(EPartida p)
        {          
            string sql = "SELECT turno FROM partida WHERE num_jugador = @num and id_mesa = @id_mesa";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@num", p.Num_jugador);
                cmd.Parameters.AddWithValue("@id_mesa", p.Id_mesa);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    p.Turno = !reader.IsDBNull(0) ? reader.GetBoolean(0) : p.Turno = false;
                    return p;
                }
            }
            return p;
        }
        public void TurnoCambio(EPartida p)
        {
            try
            {
                string sql = "UPDATE partida SET turno = @turn WHERE num_jugador = @num and id_mesa = @id_mesa";
                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@turn", p.Turno);                  
                    cmd.Parameters.AddWithValue("@num", p.Num_jugador);
                    cmd.Parameters.AddWithValue("@id_mesa", p.Id_mesa);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Problema en update: " + ex);
            }
        }
        public void AumentoApuesta(int id, int id_mesa, double apuesta)
        {
            try
            {
                string sql = "UPDATE apuestas SET apuesta = @apuest WHERE id_usuario = @id_us and id_mesa = @id_mesa";
                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@apuest", apuesta);
                    cmd.Parameters.AddWithValue("@id_us", id);
                    cmd.Parameters.AddWithValue("@id_mesa", id_mesa);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Problema en update: " + ex);
            }
        }
        public void RecibirPremio(int id, double apuesta)
        {
            try
            {
                string sql = "UPDATE usuarios SET dinero_disponible = @dinero WHERE id = @id";
                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@dinero", apuesta);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Problema en update: " + ex);
            }
        }
        public bool VerificarTurnoJug(EPartida p)
        {
            bool turno = false;
            string sql = "select turno from partida where turno = true and num_jugador = @num and id_mesa = @id_mesa order by id desc limit 1";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@num", p.Num_jugador);
                cmd.Parameters.AddWithValue("@id_mesa", p.Id_mesa);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    turno = !reader.IsDBNull(0) ? reader.GetBoolean(0) : turno = false;
                    return turno;
                }
            }
            return turno;
        }
        public bool VerificarApuesta(EApuesta p)
        {
            string sql = "select apuesta from apuestas where id_usuario = @id_us and id_mesa = @id_mesa";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id_us", p.Id_usuario);
                cmd.Parameters.AddWithValue("@id_mesa", p.Id_mesa);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    p.apuesta = !reader.IsDBNull(0) ? reader.GetDouble(0) : p.apuesta = 0;
                    return true;
                }
            }
            return false;
        }
        public double VerificarInvertido(EApuesta p)
        {
            string sql = "select sum(apuesta) from apuestas where id_usuario = @id_us and id_mesa = @id_mesa";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id_us", p.Id_usuario);
                cmd.Parameters.AddWithValue("@id_mesa", p.Id_mesa);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    p.apuesta = !reader.IsDBNull(0) ? reader.GetDouble(0) : p.apuesta = 0;
                    return p.apuesta;
                }
            }
            return p.apuesta;
        }
        public double ApuestaEnMesa(EApuesta m)
        {
            string sql = "SELECT apuesta FROM apuestas WHERE id_usuario = @id_us and id_mesa = @id_mesa";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id_us",m.Id_usuario );
                cmd.Parameters.AddWithValue("@id_mesa", m.Id_mesa);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    m.apuesta = !reader.IsDBNull(0) ? reader.GetDouble(0) : m.apuesta = 0;
                    return m.apuesta;
                }
            }
            return m.apuesta;
        }
        public int Total(ECarta m)
        {
            int sum = 0;
            string sql = "SELECT sum(valor_num) FROM cartas WHERE id_mesa = @id_mesa and id_jugador = @id_jug";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id_mesa", m.Id_Mesa);
                cmd.Parameters.AddWithValue("@id_jug", m.Id_jugador);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    sum = !reader.IsDBNull(0) ? reader.GetInt32(0) : sum = 0;
                    return sum;
                }
            }
            return sum;
        }
    }
}
