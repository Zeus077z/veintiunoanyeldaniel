﻿namespace veintiuno_utn.GUI
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMenu));
            this.btnPartidaPrivada = new System.Windows.Forms.PictureBox();
            this.btnPartidaAbierta = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.btnPartidaPrivada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPartidaAbierta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPartidaPrivada
            // 
            this.btnPartidaPrivada.BackColor = System.Drawing.Color.Transparent;
            this.btnPartidaPrivada.Image = global::veintiuno_utn.Properties.Resources.partida_privada__3_;
            this.btnPartidaPrivada.Location = new System.Drawing.Point(313, 333);
            this.btnPartidaPrivada.Name = "btnPartidaPrivada";
            this.btnPartidaPrivada.Size = new System.Drawing.Size(222, 87);
            this.btnPartidaPrivada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPartidaPrivada.TabIndex = 4;
            this.btnPartidaPrivada.TabStop = false;
            this.btnPartidaPrivada.Click += new System.EventHandler(this.BtnPartidaPrivada_Click);
            // 
            // btnPartidaAbierta
            // 
            this.btnPartidaAbierta.BackColor = System.Drawing.Color.Transparent;
            this.btnPartidaAbierta.Image = global::veintiuno_utn.Properties.Resources.partida_abierta__3_;
            this.btnPartidaAbierta.Location = new System.Drawing.Point(313, 221);
            this.btnPartidaAbierta.Name = "btnPartidaAbierta";
            this.btnPartidaAbierta.Size = new System.Drawing.Size(222, 84);
            this.btnPartidaAbierta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPartidaAbierta.TabIndex = 3;
            this.btnPartidaAbierta.TabStop = false;
            this.btnPartidaAbierta.Click += new System.EventHandler(this.BtnPartidaAbierta_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Image = global::veintiuno_utn.Properties.Resources.Blackjack_editado2;
            this.pictureBox1.Location = new System.Drawing.Point(-3, -3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(871, 609);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.PictureBox1_Click);
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::veintiuno_utn.Properties.Resources.Blackjack_editado2__1_;
            this.ClientSize = new System.Drawing.Size(869, 600);
            this.Controls.Add(this.btnPartidaPrivada);
            this.Controls.Add(this.btnPartidaAbierta);
            this.Controls.Add(this.pictureBox1);
            this.Name = "FrmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmMenu";
            this.Load += new System.EventHandler(this.FrmMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btnPartidaPrivada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPartidaAbierta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox btnPartidaAbierta;
        private System.Windows.Forms.PictureBox btnPartidaPrivada;
    }
}