﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using veintiuno_utn.Entities;

namespace veintiuno_utn.GUI
{
    public partial class FrmMenu : Form
    {
        public EUsuario u;
        public FrmMenu(EUsuario u)
        {
            InitializeComponent();
            this.u = u;
            btnPartidaAbierta.Parent = pictureBox1;
            btnPartidaPrivada.Parent = pictureBox1;

        }
      
        private void BtnPartidaAbierta_Click(object sender, EventArgs e)
        {
            FrmCartas frm = new FrmCartas(u,"1");
            frm.ShowDialog();
            this.Close();
        }

        private void BtnPartidaPrivada_Click(object sender, EventArgs e)
        {
            FrmCartas frm = new FrmCartas(u, "2");
            frm.ShowDialog();
            this.Close();

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void FrmMenu_Load(object sender, EventArgs e)
        {
            btnPartidaAbierta.Parent = pictureBox1;
            btnPartidaPrivada.Parent = pictureBox1;

        }
    }


   
    }

