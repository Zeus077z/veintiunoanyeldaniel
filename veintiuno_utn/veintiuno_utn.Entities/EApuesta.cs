﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace veintiuno_utn.Entities
{
    public class EApuesta
    {
        public int Id { get; set; }
        public int Id_usuario { get; set; }
        public int Id_mesa { get; set; }
        public double apuesta { get; set; }
    }
}
