﻿using com.sun.tools.@internal.ws.processor.model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebService;
using veintiuno_utn.Entities;
using System.Xml.Linq;
using veintiuno_utn.BOL;
using veintiuno_utn.DAL;
using System.Threading;
using com.sun.xml.@internal.ws.api.server;

namespace veintiuno_utn.GUI
{
    public partial class FrmCartas : Form
    {
        private DeckDAL ddal;
        private List<XElement> li;
        private List<string> listaValores;
        private List<string> listaPng;
        private List<PictureBox> ptbs;
        private int nuevas_cartas;
        private XDocument xml1;
        private string codigoDeck;
        private int Restantes;
        public Uri Url { get; }
        public FrmCartas()
        {
            InitializeComponent();
            ddal = new DeckDAL();
            li = new List<XElement>();
            listaValores = new List<string>();
            listaPng = new List<string>();
            ptbs = new List<PictureBox>();
            nuevas_cartas = 1;
        }

        private void frmCartas_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
            guardarPtbs();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
        }
            
        private void btnUnirse_Click(object sender, EventArgs e)
        {
            nuevo();
            string codigo = txtCodigo.Text.Trim();
            bool estado = ddal.VerificarMesa(codigo);
            if (estado == true)
            {
                string draw = String.Format("https://deckofcardsapi.com/api/deck/" + "{0}" + "/draw/?count=2", codigo);
                var json2 = new WebClient().DownloadString(draw);
                var test = json2.ParseJSON<Rootobject>();
                XNode node = JsonConvert.DeserializeXNode(json2, "Root");
                string xml = node.ToString();
                XDocument xml1 = XDocument.Parse(xml);
                string valor = xml1.Descendants("remaining").ToList().Last().Value;
                int valorRem = Int32.Parse(valor);

                ddal.CantidadCartas(valorRem, codigo);

                lblFaltantes.Text = valor;

                li = xml1.Descendants("png").ToList();
                listaValores = new List<string>();
                foreach (var item in li)
                {
                    string dato = item.Value;
                    listaValores.Add(dato);
                }
                ptbCarta1.ImageLocation = String.Format("{0}", listaValores[0]);
                ptbCarta1.Visible = false;
                ptbCarta2.Visible = true;

            }
            Derecha();

        }
        private void nuevo()
        {
            ptbCarta1.Image = null;
            if (!ptbCarta1.Bounds.X.ToString().Equals("-177"))
            {
                ptbCarta1.SetBounds(ptbCarta1.Bounds.X - 250, ptbCarta1.Bounds.Y, ptbCarta1.Width, ptbCarta1.Height);
            }
            listaValores.Clear();
            listaPng.Clear();
            li.Clear();

        }
        private int suma(List<string> valores)
        {
            int total = 0;
            string num = "0";
            for (int q = 0; q < valores.Count; q++)
            {
                num = valores[q];
                if (valores[q].Equals("QUEEN") || valores[q].Equals("JACK") || valores[q].Equals("KING"))
                {
                    num = "10";
                }
                else if (valores[q].Equals("ACE"))
                {
                    num = "11";
                }
                total += Int32.Parse(num);
            }
            return total;
        }

        private void Derecha()
        {
            if (ptbCarta1.Height + ptbCarta1.Bounds.X < Height)
            {
                for (int i = 0; i < 250; i++)
                {
                    ptbCarta1.SetBounds(ptbCarta1.Bounds.X + 1, ptbCarta1.Bounds.Y, ptbCarta1.Width, ptbCarta1.Height);
                    Thread.Sleep(1);
                    Refresh();
                }
                // MessageBox.Show(ptbCarta2.Location.ToString());
                lblMess.Visible = false;
            }
        }

        private void guardarPtbs()
        {
            ptbs.Add(ptbCarta1);
            ptbs.Add(ptbCarta2);
            ptbs.Add(ptbCarta3);
            ptbs.Add(ptbCarta4);
            ptbs.Add(ptbCarta5);
            ptbs.Add(ptbCarta6);
            ptbs.Add(ptbCarta7);
            ptbs.Add(ptbCarta8);
            ptbs.Add(ptbCarta9);
            ptbs.Add(ptbCarta10);
            ptbs.Add(ptbCarta11);
            ptbs.Add(ptbCarta12);
            ptbs.Add(ptbCarta13);
        }

        private void btnNewCarta_Click(object sender, EventArgs e)
        {
            try
            {
                nuevas_cartas += 1;
                ptbs[nuevas_cartas].Visible = true;
            }
            catch (Exception)
            {
                lblMess.Text = "No puede pedir mas cartas!";
            }
        }

        private void ptbs_Click(object sender, EventArgs e)
        {
            PictureBox carta = (PictureBox)sender;
            int pos = Convert.ToInt32(carta.AccessibleName);
            if (pos != 0 && carta.AccessibleDescription != "Used")
            {
                string draw = String.Format("https://deckofcardsapi.com/api/deck/" + "{0}" + "/draw/?count=1", codigoDeck);
                var json = new WebClient().DownloadString(draw);
                var test1 = json.ParseJSON<Rootobject>();
                XNode node1 = JsonConvert.DeserializeXNode(json, "Root");
                string xml = node1.ToString();
                xml1 = XDocument.Parse(xml);
                string valor1 = xml1.Descendants("remaining").ToList().Last().Value;
                int valorRem = Int32.Parse(valor1);
                ddal.CantidadCartas(valorRem, codigoDeck);
                lblFaltantes.Text = valorRem.ToString();
                li = xml1.Descendants("png").ToList();
                foreach (var item in li)
                {
                    string dato = item.Value;
                    listaPng.Add(dato);
                }
                carta.ImageLocation = String.Format("{0}", listaPng[pos]);
                li = xml1.Descendants("value").ToList();
                foreach (var item in li)
                {
                    string dato1 = item.Value;
                    listaValores.Add(dato1);
                }
                carta.AccessibleDescription = "Used";
                lblTotal.Text = "Valor total = " + suma(listaValores);
            }
            else
            {
                MessageBox.Show("Ya se reveló el valor de esta carta");
            }
        }

        private void btnNuevo_Click_1(object sender, EventArgs e)
        {
                Cursor = Cursors.AppStarting;
                nuevo();
                ptbCarta2.Visible = true;
                lblMess.Visible = true;
                lblPass.Text = Password.GenerarPassword(8);
                EMesas m = new EMesas()
                {
                    Codigo_invitacion = lblPass.Text,
                    Partida_en_curso = true
                };
                ddal.InsertarMesa(m);
                int id = ddal.IdMesa(lblPass.Text);
                for (int i = 0; i < 6; i++)
                {
                    var json2 = new WebClient().DownloadString("https://deckofcardsapi.com/api/deck/new/draw/?count=0");
                    var test = json2.ParseJSON<Rootobject>();
                    XNode node = JsonConvert.DeserializeXNode(json2, "Root");
                    string XML = node.ToString();
                    xml1 = XDocument.Parse(XML);
                    lblXML.Text = XML;
                    string deck = xml1.Descendants("deck_id").ToList().Last().Value;
                    string valor = xml1.Descendants("remaining").ToList().Last().Value;
                    int remaining = Int32.Parse(valor);
                    EDecks d = new EDecks()
                    {
                        Codigo = deck,
                        Id_mesa = id,
                        Cartas_restantes = remaining
                    };
                    ddal.InsertarDecks(d);
                }
                EMesas me = new EMesas()
                {
                    Codigo_invitacion = lblPass.Text
                };
                ddal.CargarMesaActual(me);
                EDecks d1 = new EDecks()
                {
                    Id_mesa = me.Id
                };
                ddal.DeckAUsar(d1);
                codigoDeck = d1.Codigo;
                Restantes = d1.Cartas_restantes;
                string draw = String.Format("https://deckofcardsapi.com/api/deck/" + "{0}" + "/draw/?count=1", d1.Codigo);
                var json = new WebClient().DownloadString(draw);
                var test1 = json.ParseJSON<Rootobject>();
                XNode node1 = JsonConvert.DeserializeXNode(json, "Root");
                string xml = node1.ToString();
                xml1 = XDocument.Parse(xml);
                string valor1 = xml1.Descendants("remaining").ToList().Last().Value;
                int valorRem = Int32.Parse(valor1);
                ddal.CantidadCartas(valorRem, d1.Codigo);
                lblFaltantes.Text = d1.Cartas_restantes.ToString();
                li = xml1.Descendants("png").ToList();
                foreach (var item in li)
                {
                    string dato = item.Value;
                    listaPng.Add(dato);
                }
                li = xml1.Descendants("value").ToList();
                foreach (var item in li)
                {
                    string dato1 = item.Value;
                    listaValores.Add(dato1);
                }
                ptbCarta1.ImageLocation = String.Format("{0}", listaPng[0]);
                Derecha();
                lblMess.Visible = false;
                btnNewCarta.Visible = true;
                Cursor = Cursors.Default;
            }
        }
    }
